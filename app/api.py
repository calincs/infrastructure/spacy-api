from collections import defaultdict
import os

from dotenv import load_dotenv, find_dotenv
from fastapi import Body, FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse
import spacy
import srsly

from app.models import (
    ENT_PROP_MAP,
    RecordsRequest,
    RecordsResponse,
    RecordsEntitiesByTypeResponse,
)
from app.spacy_extractor import SpacyExtractor

app = FastAPI(
    title="spaCy FastAPI",
    version="1.1",
    description="spaCy FastAPI for LINCS",
)

example_request = srsly.read_json("app/data/example_request.json")

enModel = spacy.load("en_core_web_trf")
enExtractor = SpacyExtractor(enModel)
frModel = spacy.load("fr_core_news_md")
frExtractor = SpacyExtractor(frModel)


@app.get("/", include_in_schema=False)
def docs_redirect():
  return RedirectResponse(f"/docs")


@app.post("/entities", tags=["NER"])
async def extract_entities(body: RecordsRequest = Body(..., example=example_request)):
  """Extract Named Entities from a batch of Records."""

  res = []
  documents = []

  for val in body.values:
    documents.append({"id": val.recordId, "text": val.text})

  entities_res = None
  if body.language == "fr":
    entities_res = frExtractor.extract_entities(documents)
  else:
    # Default to enExtractor if language is not "fr"
    entities_res = enExtractor.extract_entities(documents)

  res = [{"recordId": er["id"], "data": {"entities": er["entities"]}} for er in entities_res]

  return {"values": res}


@app.post("/entities_by_type", tags=["NER"])
async def extract_entities_by_type(body: RecordsRequest = Body(..., example=example_request)):
  """Extract Named Entities from a batch of Records separated by entity label."""

  res = []
  documents = []

  for val in body.values:
    documents.append({"id": val.recordId, "text": val.text})

  if body.language == "fr":
    entities_res = frExtractor.extract_entities(documents)
  else:
    # Default to enExtractor if language is not "fr"
    entities_res = enExtractor.extract_entities(documents)

  res = []

  for er in entities_res:
    groupby = defaultdict(list)
    for ent in er["entities"]:
      ent_prop = ENT_PROP_MAP[ent["label"]]
      groupby[ent_prop].append(ent["name"])
    record = {"recordId": er["id"], "data": groupby}
    res.append(record)

  return {"values": res}
