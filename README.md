# spaCy FastAPI

spaCy FastAPI for LINCS

Built with `https://github.com/microsoft/cookiecutter-spacy-fastapi`

---

## Resources

This project has two key dependencies:

| Dependency Name | Documentation                  | Description                                                                            |
|-----------------|--------------------------------|----------------------------------------------------------------------------------------|
| spaCy           | [https://spacy.io]             | Industrial-strength Natural Language Processing (NLP) with Python and Cython           |
| FastAPI         | [https://fastapi.tiangolo.com] | FastAPI framework, high performance, easy to learn, fast to code, ready for production |

---

## Build and run

### Locally

To run locally in debug mode:

```bash
cd ./spacy-fastapi
bash ./create_virtualenv.sh
uvicorn app.api:app --reload
```

### Or with Docker

```bash
docker compose up
```

### API access

Open your browser to [http://localhost:5000/docs](http://localhost:8000/docs) to view the OpenAPI UI.

![Open API Image](./images/cookiecutter-docs.png)

For an alternate view of the docs navigate to [http://localhost:5000/redoc](http://localhost:8000/redoc)
